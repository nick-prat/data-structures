#ifndef __BPLUS_H
#define __BPLUS_H

#include <array>
#include <memory>

template<typename T, int size>
class BPlusTree {

    template<typename... args_t>
    void insert(args_t&&... args) {
        std::unique_ptr<T> ptr{std::forward<args_t>(args)...};
    }

    void insert(T* tPtr) {

    }

private:
    struct BPlusNode {
        std::array<std::unique_ptr<T>, size> elements;
        std::array<std::unique_ptr<BPlusNode>, size+1> nodes;
    };

    std::unique_ptr<BPlusNode> m_root;
};

#endif // __BPLUS_H
