#include <iostream>
#include <vector>

#include "linkedlist.hh"

class A {
public:
    A(int val)
    : m_val{val} {}

    A(A const& a)
    : m_val{a.m_val} {}

    A(A&& a)
    : m_val{a.m_val} {
        a.m_val = 0;
    }

    bool operator==(A const& a) {
        return m_val == a.m_val;
    }

    int getVal() const {
        return m_val;
    }

    void setVal(int val) {
        m_val = val;
    }

private:
    int m_val;
};

int main(int argc, char** argv) {
    LinkedList<int> ll;

    auto iter1 = ll.insertFront(10);
    auto iter2 = ll.insertFront(20);
    auto iter3 = ll.insertBack(30);

    std::cout << ll.size() << '\n';

    for(auto& i : ll) {
        std::cout << i << '\n';
    }

}
