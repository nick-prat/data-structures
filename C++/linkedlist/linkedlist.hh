#ifndef __LINKED_LIST_H
#define __LINKED_LIST_H

#include <memory>

template<typename T>
class LinkedList {
private:
    struct Node {
        Node() = default;
        Node(Node const& node) = delete;
        Node(Node&& node) = default;

        template<typename... args_t>
        Node(args_t&&... args)
        : m_next{nullptr}
        , m_prev{nullptr}
        , m_data{std::forward<args_t>(args)...} {}

        Node* m_next;
        Node* m_prev;
        T m_data;
    };

public:
    class Iterator;

    class ConstIterator {
        friend class Iterator;
        friend class LinkedList<T>;
    public:
        ConstIterator() = default;
        ConstIterator(ConstIterator const&) = default;
        ConstIterator(ConstIterator&&) = default;

        ConstIterator(Node* node)
        : m_node{node} {}

        ConstIterator(Iterator const& iter)
        : m_node{iter.m_node} {}

        ConstIterator(Iterator&& iter)
        : m_node{iter.m_node} {
            iter.m_node = nullptr;
        }

        ConstIterator& operator=(ConstIterator const& iter) {
            m_node = iter.m_node;
        }

        ConstIterator& operator=(Iterator const& iter) {
            m_node = iter.m_node;
        }

        ConstIterator& operator++() {
            m_node = m_node->m_next;
            return *this;
        }

        ConstIterator& operator++(int) {
            ConstIterator iter{*this};
            m_node = m_node->m_next;
            return iter;
        }

        ConstIterator& operator--() {
            m_node = m_node->m_prev;
            return *this;
        }

        ConstIterator& operator--(int) {
            ConstIterator iter{*this};
            m_node = m_node->m_prev;
            return iter;
        }

        bool operator==(ConstIterator const& iter) const {
            return m_node == iter.m_node;
        }

        bool operator==(Iterator const& iter) const {
            return m_node == iter.m_node;
        }

        bool operator!=(ConstIterator const& iter) const {
            return m_node != iter.m_node;
        }

        bool operator!=(Iterator const& iter) const {
            return m_node != iter.m_node;
        }

        T const& operator*() const {
            return m_node->m_data;
        }

        T const* operator->() const {
            return &(m_node->m_data);
        }

    private:
        Node const* m_node;
    };

    class Iterator {
        friend class ConstIterator;
        friend class LinkedList<T>;
    public:
        Iterator() = default;
        Iterator(Iterator const& iter) = default;
        Iterator(Iterator&& iter) = default;

        Iterator(Node* node)
        : m_node{node} {}

        Iterator& operator=(Iterator const& iter) {
            m_node = iter.m_node;
            return *this;
        }

        Iterator& operator++() {
            m_node = m_node->m_next;
            return *this;
        }

        Iterator operator++(int) {
            Iterator iter{*this};
            m_node = m_node->m_next;
            return iter;
        }

        Iterator& operator--() {
            m_node = m_node->m_prev;
            return *this;
        }

        Iterator operator--(int) {
            Iterator iter{*this};
            m_node = m_node->m_prev;
            return iter;
        }

        bool operator==(Iterator const& iter) const {
            return m_node == iter.m_node;
        }

        bool operator==(ConstIterator const& iter) const {
            return m_node == iter.m_node;
        }

        bool operator!=(Iterator const& iter) const {
            return m_node != iter.m_node;
        }

        bool operator!=(ConstIterator const& iter) const {
            return m_node != iter.m_node;
        }

        T& operator*() const {
            return m_node->m_data;
        }

        T* operator->() const {
            return &(m_node->m_data);
        }

    private:
        Node* m_node;
    };

public:
    LinkedList()
    : m_head{nullptr}
    , m_tail{nullptr} {};

    ~LinkedList() {
        auto node = m_head;
        while(node != nullptr) {
            auto next = node->m_next;
            delete node;
            node = next;
        }
    }

    template<typename... args_t>
    Iterator insertBack(args_t&&... args) {
        if(m_head == nullptr) {
            m_head = new Node{std::forward<args_t>(args)...};
            m_tail = m_head;
        } else {
            auto node = new Node{std::forward<args_t>(args)...};
            m_tail->m_next = node;
            node->m_prev = m_tail;
            m_tail = node;
        }
        return Iterator{m_tail};
    }

    template<typename... args_t>
    Iterator insertFront(args_t&&... args) {
        if(m_head == nullptr) {
            m_head = new Node{std::forward<args_t>(args)...};
            m_tail = m_head;
        } else {
            auto node = new Node{std::forward<args_t>(args)...};
            node->m_next = m_head;
            m_head->m_prev = node;
            m_head = node;
        }
        return Iterator{m_head};
    }

    void remove(ConstIterator const& iter) {
        if(iter == cbegin() && iter == cend()) {
            m_head = m_tail = nullptr;
        } else if(iter == cbegin()) {
            m_head = iter.m_node->m_next;
        } else if(iter == cend()) {
            m_tail = iter.m_node->m_prev;
        } else {
            iter.m_node->m_prev->m_next = iter.m_node->m_next;
        }
        delete iter.m_node;
    }

    bool empty() const {
        return m_head == nullptr;
    }

    int size() const {
        int i = 0;
        for(auto node = m_head; node != nullptr; node = node->m_next)
            i++;
        return i;
    }

    Iterator begin() {
        return {m_head};
    }

    Iterator end() {
        return {};
    }

    ConstIterator cbegin() const {
        return {m_head};
    }

    ConstIterator cend() const {
        return {};
    }

private:
    Node* m_head;
    Node* m_tail;
};

#endif // __LINKED_LIST_H
