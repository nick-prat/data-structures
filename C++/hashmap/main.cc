#include <iostream>
#include <vector>
#include <unordered_map>
#include "hash.hh"

class A {
    // A(A&& a) = delete;
public:
    A() {}
    A(A const&) = default;
    A(A&& a) = default;

    A& operator=(A const&) = default;
};

class B {
public:
    B() = default;
    B(B const&) = delete;
    B(B&&) {
        std::cout << "move const\n";
    };
};

class C {
public:
    C() = default;
    C(int) {}
    C(C const&) {
        std::cout << "copy const\n";
    };
    C(C&&) = delete;

    C& operator=(C const&) = default;
};

void func1(C&& c) {
    std::cout << "rvalue\n";
}

void func1(C const& c) {
    std::cout << "lvalue\n";
}

int main(int argc, char** argv) {
    C c{1};
    std::unordered_map<int, C> m;
    m[1] = c;
    func1(c);
    func1(std::move(c));

    if constexpr(std::is_move_constructible<std::pair<B, B>>::value) {
        std::cout << "std::pair<B, B> is move constructible\n";
    }

    if constexpr(std::is_move_constructible<std::pair<C, C>>::value) {
        std::cout << "std::pair<C, C> is move constructible\n";
    }

    HashMap<int, int> hm{10};
    for(int i = 0; i < 200; i++) {
        hm[i] = i;
    }
    hm.printBuckets();
}
