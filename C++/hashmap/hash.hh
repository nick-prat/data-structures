#ifndef __HASHMAP_H
#define __HASHMAP_H

#include <vector>
#include <list>
#include <iostream>

template<typename key_t, typename val_t>
class Bucket {
private:
    using pair_t = std::pair<key_t, val_t>;

public:
    void insert(pair_t&& pair) {
        if constexpr(std::is_move_constructible<pair_t>::value) {
            m_elements.push_back(std::move(pair));
        } else if constexpr(std::is_copy_constructible<pair_t>::value){
            m_elements.push_back(pair);
        }
    }

    val_t& get(key_t const& key) {
        for(auto& element : m_elements) {
            if(key == element.first) {
                return element.second;
            }
        }
        throw std::out_of_range{"Key doesn't exist"};
    }

    val_t const& get(key_t const& key) const {
        for(auto& element : m_elements) {
            if(key == element.first) {
                return element.second;
            }
        }
        throw std::out_of_range{"Key doesn't exist"};
    }

    val_t& getOrInsert(key_t const& key) {
        if(!contains(key)) {
            insert(std::pair{key, val_t{}});
        }
        return get(key);
    }

    bool contains(key_t const& key) const {
        for(auto const& element : m_elements) {
            if(key == element.first) return true;
        }
        return false;
    }

    void printElements() const {
        std::cout << "Bucket: ";
        for(auto const& element : m_elements) {
            std::cout << '{' << element.first << ',' << element.second << "} ";
        }
        std::cout << '\n';
    }

private:
    std::list<pair_t> m_elements;
};

template<typename key_t, typename val_t, typename bucket_t = Bucket<key_t, val_t>>
class HashMap {
private:
    using pair_t = std::pair<key_t, val_t>;
    auto hash(key_t const& key) {
        return m_hash(key) % m_buckets.capacity();
    }

public:
    HashMap(size_t capacity)
    : m_buckets{capacity}
    , m_hash{} {}

    void insert(pair_t&& pair) {
        m_buckets[hash(pair.first)].insert(std::move(pair));
    }

    val_t& get(key_t const& key) {
        return m_buckets[hash(key)].get(key);
    }

    val_t const& get(key_t const& key) const {
        return m_buckets[hash(key)].get(key);
    }

    val_t& operator[](key_t const& key) {
        return m_buckets[hash(key)].getOrInsert(key);
    }

    val_t const& operator[](key_t const& key) const {
        return m_buckets[hash(key)].get(key);
    }

    void printBuckets() const {
        for(auto const& bucket : m_buckets) {
            bucket.printElements();
        }
    }

private:
    std::vector<bucket_t> m_buckets;
    std::hash<key_t> m_hash;
};

#endif // __HASHMAP_H
