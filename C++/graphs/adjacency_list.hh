#pragma once

#include <stdexcept>
#include <utility>
#include <list>
#include <unordered_map>

template<typename s>
class adjacency_list {
public:
    bool has_vertex(s const& vertex) {
        return edges.find(vertex) != edges.end();
    }

    void create_vertex(s const& vertex) {
        edges.insert({vertex, {}});
    }

    // void remove_vertex(s const& src) {
    //     edges.erase(src);
    // }

    void remove_edge(s const& src, s const& dst) {
        auto list_iter = edges.find(src);
        if ( list_iter == edges.end() )
            return;

        for ( auto iter = list_iter->second.begin(); iter != list_iter->second.end(); iter++ ) {
            if ( *iter == dst ) {
                list_iter->second.erase(iter);
                return;
            }
        }
    }

    std::list<s> const& getEdges(s const& src) const {
        auto l = edges.find(src);
        if ( l == edges.end() )
            throw std::runtime_error{"Adjacency list missing vertex"};
        return l->second;
    }

    void insert_edge(s const& src, s const& dst) {
        auto list_iter = edges.find(src);
        if ( list_iter == edges.end() ) {
            list_iter = edges.insert({src, {}}).first;
        }
        list_iter->second.push_front(dst);
        list_iter = edges.find(dst);
        if ( list_iter == edges.end() ) {
            edges.insert({dst, {}});
        }
    }

    bool is_connected(s const& src, s const& dst) const {
        auto iter = edges.find(src);
        for ( auto const& v : iter.second )
            if ( v == dst ) return true;
        return false;
    }

private:
    std::unordered_map<s, std::list<s>> edges;
};