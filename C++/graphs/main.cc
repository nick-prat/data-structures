#include "adjacency_list.hh"

#include <functional>
#include <queue>
#include <stack>
#include <unordered_set>
#include <set>
#include <functional>
#include <iostream>

namespace std {
    template<typename t>
    struct std::hash<std::reference_wrapper<t>> {
        std::size_t operator()(std::reference_wrapper<t> const& a) const {
            return std::hash<typename std::remove_const<t>::type>{}(a.get());
        }
    };
}

template<typename s>
bool breadth_first_search(adjacency_list<s> const& adjl, s const& src, s const& dst) {
    std::queue<std::reference_wrapper<s const>> queue{};
    std::unordered_set<std::reference_wrapper<s const>> visited{};
    
    queue.push(src);
    visited.insert(src);

    while( ! queue.empty() ) {
        auto& val = queue.front();
        queue.pop();
        visited.insert(val);

        if ( val == dst ) return true;
        for ( auto const& vert : adjl.getEdges(val) ) {
            if ( visited.find(vert) == visited.end() )
                queue.push(vert);
        }
    } 

    return false;
}

template<typename s>
bool depth_first_search(adjacency_list<s> const& adjl, s const& src, s const& dst) {
    std::stack<std::reference_wrapper<s const>> stack{};
    std::unordered_set<std::reference_wrapper<s const>> visited{};

    stack.push(src);
    visited.insert(src);

    while( ! stack.empty() ) {
        auto& val = stack.top();
        stack.pop();
        visited.insert(val);

        if ( val == dst ) return true;
        for ( auto const& vert : adjl.getEdges(val) ) {
            if ( visited.find(vert) == visited.end() )
                stack.push(vert);
        }
    }

    return false;
}

int main() {
    adjacency_list<int> adjl{};
    adjl.insert_edge(3, 4);
    adjl.insert_edge(4, 3);
    adjl.insert_edge(3, 5);
    adjl.insert_edge(3, 6);
    adjl.insert_edge(3, 7);
    adjl.insert_edge(6, 8);
    adjl.insert_edge(1, 2);

    int a = 4;
    std::unordered_set<std::reference_wrapper<int const>> set{};
    set.insert(a);
    std::cout << (set.find(a) != set.end()) << '\n';
    std::reference_wrapper<int const> ar = a;
    std::cout << (ar.get() == a) << '\n';

    std::cout << breadth_first_search(adjl, 3, 8) << '\n';
    std::cout << depth_first_search(adjl, 3, 4) << '\n';
}