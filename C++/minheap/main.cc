
#include <iostream>
#include "minheap.hh"

int main(int argc, char** argv) {
    auto mh = min_heap<int>{10};
    mh.insert(5);
    mh.print_heap(std::cout);
    mh.insert(4);
    mh.print_heap(std::cout);
    mh.insert(3);
    mh.print_heap(std::cout);
    mh.insert(2);
    mh.print_heap(std::cout);
    mh.extract_min();
    mh.print_heap(std::cout);
    mh.insert(1);
    mh.print_heap(std::cout);
    mh.extract_min();
    mh.print_heap(std::cout);
    mh.insert(0);
    mh.print_heap(std::cout);
}