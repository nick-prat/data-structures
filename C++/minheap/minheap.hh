#pragma once

#include <type_traits>
#include <vector>
#include <iostream>

template<typename T>
class min_heap {
    using size_type = typename std::vector<T>::size_type;
public:
    min_heap(size_type size) {
        heap.resize(size);
    }

    void insert(T&& element) {
        heap[top] = std::move(element);
        bubble_down(top++);
    }

    void get_min() {
        return heap[0];
    }

    T extract_min() {
        swap(0, top-1);
        auto val = std::move(top--);
        bubble_up(0);
        return val;
    }

    void print_heap(std::ostream& stream) {
        for ( size_type  i = 0; i < top; i++ )
            stream << heap[i] << ' ';
        stream << '\n';
    }

private:
    void swap(size_type l, size_type r) {
        auto tmp = std::move(heap[r]);
        heap[r] = std::move(heap[l]);
        heap[l] = std::move(tmp);
    }

    void bubble_down(size_type i) {
        auto p = parent(i);
        if ( i == 0 || heap[i] > heap[p] )
            return;
        else {
            swap(i, p);
            bubble_down(p);
        }
    }

    void bubble_up(size_type i) {
        auto l = lchild(i);
        auto r = rchild(i);
        if ( l < top && heap[i] > heap[l] ) {
            swap(i, l);
            bubble_up(l);
        } else if ( r < top && heap[i] > heap[r] ) {
            swap(i, r);
            bubble_up(r);
        }
    }

    size_type parent(size_type i) {
        return (i - 1) / 2;
    }

    size_type lchild(size_type i) {
        return i * 2 + 1;
    }

    size_type rchild(size_type i) {
        return i * 2 + 2;
    }

private:
    std::vector<T> heap;
    size_type top = 0;
};