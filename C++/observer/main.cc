#include "observer.hh"

#include <iostream>

int main(int argc, char** argv) {
    auto a = 4;
    auto obs = observer(a);
    
    auto l = listener<int>{[](int& a) {
        std::cout << a << '\n';
    }};
    obs.add_listener(l);

    std::cout << "Testing\n";

    obs.set(1);
    obs.set(2);
    obs.set(3);

    obs.remove_listener(l);

    std::cout << "foo\n";

    obs.set(4);

    return 0;
}