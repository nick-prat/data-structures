#pragma once

#include <functional>
#include <list>

template<typename t>
class observer;

template<typename t>
class listener {
    friend class observer<t>;
    using callback_t = std::function<void(t&)>;

public:
    listener(callback_t callback)
    : callback{callback}, m_prev{nullptr}, m_next{nullptr} {};

    listener(listener<t> const&) = delete;
    listener(listener<t>&&) = default;

    void notify(t& data) {
        std::invoke(callback, data);
    }

private:
    callback_t callback;
    listener<t>* m_prev;
    listener<t>* m_next;
};

template<typename t>
class observer {
public:
    observer(t& data)
    : m_data{data}, m_listeners{nullptr} {}

    observer(observer const&) = delete;
    observer(observer&&) = default;

    void set(t& data) {
        m_data = data;
        notify();
    }

    void set(t&& data) {
        m_data = std::move(data);
        notify();
    }

    void add_listener(listener<t>& l) {
        if ( m_listeners != nullptr ) {
            l.m_next = m_listeners;
            l.m_next->m_prev = &l;
        }
        
        m_listeners = &l;
    }

    void remove_listener(listener<t>& l) {
        if ( m_listeners == nullptr )
            return;
        
        if ( m_listeners == &l ) {
            m_listeners = l.m_next;
            if ( m_listeners != nullptr )
                m_listeners->m_prev = nullptr;
        } else {
            if ( l.m_prev != nullptr )
                l.m_prev->m_next = l.m_next;
            if ( l.m_next != nullptr )
                l.m_next->m_prev = l.m_prev;
        }
    }

private:
    void notify() {
        for ( auto l = m_listeners; l != nullptr; l = l->m_next )
            l->notify(m_data);
    }

private:
    t& m_data;
    listener<t>* m_listeners;
};