#include "binarytree.hh"

#include <iostream>

int main(int argc, char const** argv) {
    auto btree = BinaryTree<int>();
    btree.insert(1);
    btree.insert(2);
    btree.insert(3);
}
