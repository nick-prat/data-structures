#ifndef __BINARY_TREE_H
#define __BINARY_TREE_H

#include <utility>

template<typename T>
class BinaryTree {
private:
    struct Node {
        Node() = default;
        Node(Node const& node) = delete;
        Node(Node&& node) = default;

        template<typename... args_t>
        Node(args_t&&... args)
        : m_left{nullptr}
        , m_right{nullptr}
        , m_parent{nullptr}
        , m_height{0}
        , m_data{std::forward<args_t>(args)...} {}

        Node* m_left;
        Node* m_right;
        Node* m_parent;
        int m_height;
        T m_data;
    };

public:
    template<typename... args_t>
    void insert(args_t&&... args) {
        auto node = new Node{std::forward<args_t>(args)...};

        Node* parent = nullptr;
        auto search = m_root;

        while(search != nullptr) {
            parent = search;
            if(node->m_data < search->m_data) {
                search = search->m_left;
            } else if(node->m_data > search->m_data) {
                search = search->m_right;
            } else {
                return;
            }
        }

        node->m_parent = parent;
        search = node;

        updateHeight(search);
        balance(search);
    }

private:
    void updateHeight(Node* node) {
        if(node == nullptr)
            return;

        int newHeight = max(getHeight(node->m_left), getHeight(node->m_right)) + 1;

        if(node->m_height < newHeight) {
            node->m_height = newHeight;
            updateHeight(node->m_parent);
        }
    }

    void balance(Node* node, int height = 0) {
        Node* unbalanced = getParent(getParent(node));
        Node* child = getParent(node);
        Node* grandchild = node;

        while(unbalanced != nullptr) {
            int balance = getBalance(unbalanced);

            if(balance < -1) {
                if(grandchild == child->m_left) {
                    // Left Left
                    rightRotate(unbalanced);
                    updateHeight(unbalanced);
                } else {
                    // Left Right
                    leftRotate(child);
                    rightRotate(unbalanced);
                    updateHeight(child);
                    updateHeight(unbalanced);
                }
            } else if(balance > 1) {
                if(grandchild == child->m_right) {
                    // Right Right
                    leftRotate(unbalanced);
                } else {
                    // Right Left
                    rightRotate(child);
                    leftRotate(unbalanced);
                    updateHeight(child);
                    updateHeight(unbalanced);
                }
            }

            unbalanced = getParent(unbalanced);
            child = getParent(child);
            grandchild = getParent(grandchild);
        }
    }

    Node* getParent(Node* node) {
        if(node == nullptr) {
            return nullptr;
        } else {
            return node->m_parent;
        }
    }

    int getBalance(Node* node) {
        return getHeight(node->m_left) - getHeight(node->m_right);
    }

    int getHeight(Node* node) {
        if(node == nullptr)
            return 0;
        else
            return node->m_height;
    }

    void rightRotate(Node* root) {
        Node* pivot = root->m_left;
        root->m_left = pivot->m_right;
        pivot->m_right = root;
    }

    void leftRotate(Node* root) {
        Node* pivot = root->m_right;
        root->m_right = pivot->m_left;
        pivot->m_left = root;
    }

    int max(int a, int b) {
        return (a > b ? a : b);
    }

private:
    Node* m_root;

};

#endif // __BINARY_TREE_H
