#include "binarytree.hh"

#include <iostream>

int main(int argc, char const** argv) {
    auto btree = BinaryTree<int>();
    btree.insert(1);
    btree.printLevelOrder();
    std::cout << '\n';
    btree.insert(2);
    btree.printLevelOrder();
    std::cout << '\n';
    btree.insert(3);
    btree.printLevelOrder();
    std::cout << '\n';
    btree.insert(4);
    btree.printLevelOrder();
    std::cout << '\n';
    btree.insert(5);
    btree.printLevelOrder();
    std::cout << '\n';
    btree.insert(6);
    btree.printLevelOrder();
    std::cout << '\n';
    btree.insert(7);
    btree.printLevelOrder();
    std::cout << '\n';
    btree.insert(0);
    btree.printLevelOrder();
    std::cout << '\n';
    btree.insert(11);
    btree.printLevelOrder();
    std::cout << '\n';
    btree.insert(-3);
    btree.printLevelOrder();
}
