#ifndef __BINARY_TREE_H
#define __BINARY_TREE_H

#include <utility>
#include <iostream>

template<typename T>
class BinaryTree {
private:
    struct Node {
        Node() = default;
        Node(Node const& node) = delete;
        Node(Node&& node) = default;

        template<typename... args_t>
        Node(args_t&&... args)
        : m_left{nullptr}
        , m_right{nullptr}
        , m_parent{nullptr}
        , m_height{0}
        , m_data{std::forward<args_t>(args)...} {}

        Node* m_left;
        Node* m_right;
        Node* m_parent;
        int m_height;
        T m_data;
    };

public:
    template<typename... args_t>
    void insert(args_t&&... args) {
        auto node = new Node{std::forward<args_t>(args)...};

        Node* parent = nullptr;
        auto search = &m_root;

        while(*search != nullptr) {
            parent = *search;
            if(node->m_data < (*search)->m_data) {
                search = &(*search)->m_left;
            } else if(node->m_data > (*search)->m_data) {
                search = &(*search)->m_right;
            } else {
                return;
            }
        }

        node->m_parent = parent;
        *search = node;

        updateHeight(*search);
        balance(*search);
    }

    void printLevelOrder() {
        for(int i = 0; i <= getHeight(m_root); i++) {
            printLevel(m_root, 0, i);
        }
    }

private:
    void printLevel(Node* node, int i, int level) {
        if(node == nullptr)
            return;

        // std::cout << i << " : " << level << '\n';
        if(i == level) {
            std::cout << level << ',' << node->m_height << ',' << getBalance(node) << " : [" << node->m_data << "]";
            for(auto parent = getParent(node); parent != nullptr; parent = getParent(parent)) {
                std::cout << " -> [" << parent->m_data << ']';
            }
            std::cout << '\n';
        } else {
            printLevel(node->m_left, i + 1, level);
            printLevel(node->m_right, i + 1, level);
        }
    }

    void updateHeight(Node* node) {
        if(node == nullptr)
            return;

        int newHeight = max(getHeight(node->m_left), getHeight(node->m_right)) + 1;

        if(node->m_height != newHeight) {
            node->m_height = newHeight;
            updateHeight(node->m_parent);
        }
    }

    void balance(Node* node, int height = 0) {
        Node* unbalanced = getParent(getParent(node));
        Node* child = getParent(node);
        Node* grandchild = node;

        while(unbalanced != nullptr) {
            int balance = getBalance(unbalanced);

            if(balance < -1) {
                if(grandchild == child->m_left) {
                    // Left Left
                    rightRotate(unbalanced);
                    updateHeight(unbalanced);
                } else {
                    // Left Right
                    leftRotate(child);
                    rightRotate(unbalanced);
                    updateHeight(child);
                    updateHeight(unbalanced);
                }
                return;
            } else if(balance > 1) {
                if(grandchild == child->m_right) {
                    // Right Right
                    leftRotate(unbalanced);
                    updateHeight(unbalanced);
                } else {
                    // Right Left
                    rightRotate(child);
                    leftRotate(unbalanced);
                    updateHeight(child);
                    updateHeight(unbalanced);
                }
                return;
            }

            unbalanced = getParent(unbalanced);
            child = getParent(child);
            grandchild = getParent(grandchild);
        }

    }

    Node* getParent(Node* node) {
        if(node == nullptr) return nullptr;
        else return node->m_parent;
    }

    int getBalance(Node* node) {
        if(node == nullptr) return 0;
        return getHeight(node->m_right) - getHeight(node->m_left);
    }

    int getHeight(Node* node) {
        if(node == nullptr) return 0;
        else return node->m_height;
    }

    void rightRotate(Node* root) {
        if(root == nullptr || root->m_left == nullptr) return;
        Node* pivot = root->m_left;

        root->m_left = pivot->m_right;
        if(pivot->m_right != nullptr) pivot->m_right->m_parent = root;

        pivot->m_right = root;
        pivot->m_parent = root->m_parent;
        if(pivot->m_parent == nullptr) {
            m_root = pivot;
        } else {
            if(root == pivot->m_parent->m_right) {
                pivot->m_parent->m_right = pivot;
            } else if(root == pivot->m_parent->m_left) {
                pivot->m_parent->m_left = pivot;
            }
        }
        root->m_parent = pivot;

        updateHeight(root);
        updateHeight(pivot);
    }

    void leftRotate(Node* root) {
        if(root == nullptr || root->m_right == nullptr) return;
        Node* pivot = root->m_right;

        root->m_right = pivot->m_left;
        if(pivot->m_left != nullptr) pivot->m_left->m_parent = root;

        pivot->m_left = root;
        pivot->m_parent = root->m_parent;
        if(pivot->m_parent == nullptr) {
            m_root = pivot;
        } else {
            if(root == pivot->m_parent->m_right) {
                pivot->m_parent->m_right = pivot;
            } else if(root == pivot->m_parent->m_left) {
                pivot->m_parent->m_left = pivot;
            }
        }
        root->m_parent = pivot;

        updateHeight(root);
        updateHeight(pivot);
    }

    int max(int a, int b) {
        return (a > b ? a : b);
    }

private:
    Node* m_root;

};

#endif // __BINARY_TREE_H
