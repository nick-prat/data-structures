#pragma once

#include <ostream>
#include <queue>

int max(int a, int b);

void swap(bool& a, bool& b);

template<typename T>
class BinaryTree {
protected:
    struct Node {
        Node() = default;
        Node(Node const& node) = delete;
        Node(Node&& node) = default;

        template<typename... args_t>
        Node(args_t&&... args)
        : m_left{nullptr}
        , m_right{nullptr}
        , m_red{true}
        , m_data{std::forward<args_t>(args)...} {}

        Node* m_left;
        Node* m_right;
        Node* m_parent;
        bool m_red;
        T m_data;
    };

    Node* internalFind(T const& elem) {
        auto search = m_root;

        while ( search != nullptr ) {
            if ( elem < search->m_data )
                search = search->m_left;
            else if ( elem > search->m_data )
                search = search->m_right;
            else
                break;
        }

        return search; 
    }

    bool isLeaf(Node* node) {
        if ( node == nullptr ) return false;
        return node->m_left == nullptr && node->m_right == nullptr;
    }

    bool isLeftOf(Node* child, Node* parent) {
        return parent != nullptr && parent->m_left == child;
    }

    bool isLeftChild(Node* node) {
        return isLeftOf(node, node->m_parent);
    }

    bool isRightOf(Node* child, Node* parent) {
        return parent != nullptr && parent->m_right == child;
    }

    bool isRightChild(Node* node) {
        return isRightOf(node, node->m_parent);
    }
    
    Node* getSuccessor(Node* node) {
        if ( node == nullptr || node->m_right == nullptr ) return nullptr;
        auto search = node->m_right;
        while ( search->m_left != nullptr) search = search->m_left;
        return search;
    }

    Node* getPredecessor(Node* node) {
        if ( node == nullptr || node->m_left == nullptr ) return nullptr;
        auto search = node->m_left;
        while ( search->m_right != nullptr) search = search->m_right;
        return search;
    }

    Node* getSibling(Node* node, Node* parent) {
        if ( parent == nullptr ) return nullptr;
        return node == parent->m_left ? parent->m_right : parent->m_left;
    }

    Node* getSibling(Node* node) {
        if ( node == nullptr ) return nullptr;
        return getSibling(node, node->m_parent);
    }

    Node* getParent(Node* node) {
        if(node == nullptr) return nullptr;
        else return node->m_parent;
    }

    Node* getGrandparent(Node* node) {
        auto parent = getParent(node);
        return parent == nullptr ? nullptr : getParent(parent);
    }

    Node* getUncle(Node* node) {
        if ( node == nullptr || getParent(node) == nullptr || getGrandparent(node) == nullptr ) return nullptr;
        return ( getParent(node) == getGrandparent(node)->m_left ) 
            ? getGrandparent(node)->m_right 
            : getGrandparent(node)->m_left;   
    }

    void rightRotate(Node* root) {
        if(root == nullptr || root->m_left == nullptr) return;
        Node* pivot = root->m_left;

        root->m_left = pivot->m_right;
        if(pivot->m_right != nullptr) pivot->m_right->m_parent = root;

        pivot->m_right = root;
        pivot->m_parent = root->m_parent;
        if(pivot->m_parent == nullptr) {
            m_root = pivot;
        } else {
            if(root == pivot->m_parent->m_right) {
                pivot->m_parent->m_right = pivot;
            } else if(root == pivot->m_parent->m_left) {
                pivot->m_parent->m_left = pivot;
            }
        }
        root->m_parent = pivot;
    }

    void leftRotate(Node* root) {
        if(root == nullptr || root->m_right == nullptr) return;
        Node* pivot = root->m_right;

        root->m_right = pivot->m_left;
        if(pivot->m_left != nullptr) pivot->m_left->m_parent = root;

        pivot->m_left = root;
        pivot->m_parent = root->m_parent;
        if(pivot->m_parent == nullptr) {
            m_root = pivot;
        } else {
            if(root == pivot->m_parent->m_right) {
                pivot->m_parent->m_right = pivot;
            } else if(root == pivot->m_parent->m_left) {
                pivot->m_parent->m_left = pivot;
            }
        }
        root->m_parent = pivot;
    }

    void del(Node* root) {
        // Post order destruction of elements
        if ( root == nullptr ) return;
        if ( root->m_left != nullptr ) del(root->m_left);
        if ( root->m_right != nullptr ) del(root->m_right);
        delete root;
    }

    int getHeight(Node* node) {
        auto height = 0;
        for ( ; node != nullptr; node = node->m_parent) height++;
        return height;
    }

public:
    void printLevelOrder(std::ostream& stream) {
        auto queue = std::queue<Node*>{};

        queue.push(m_root);

        auto curHeight = 1;
        while ( ! queue.empty() ) {
            auto elem = queue.front();
            queue.pop();

            if ( elem == nullptr ) continue;
            queue.push(elem->m_left);
            queue.push(elem->m_right);

            if ( getHeight(elem) > curHeight ) {
                stream << '\n';
                curHeight = getHeight(elem);
            }

            stream << elem->m_data << (elem->m_red ? 'r' : 'b') << ' ';
        }

        stream << '\n';
    }
    
protected:
    Node* m_root;
};