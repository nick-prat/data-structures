#include "binarytree.hh"

int max(int a, int b) {
    return (a > b ? a : b);
}

void swap(bool& a, bool& b) {
    a = a ^ b;
    b = a ^ b;
    a = a ^ b;
}