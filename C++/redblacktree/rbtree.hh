#pragma once

#include "binarytree.hh"

template<typename T>
class RedBlackTree : protected BinaryTree<T> {
    using BinaryTree<T>::del;
    using BinaryTree<T>::getGrandparent;
    using BinaryTree<T>::getHeight;
    using BinaryTree<T>::getParent;
    using BinaryTree<T>::getPredecessor;
    using BinaryTree<T>::getSibling;
    using BinaryTree<T>::getUncle;
    using BinaryTree<T>::internalFind;
    using BinaryTree<T>::isLeftChild;
    using BinaryTree<T>::isLeftOf;
    using BinaryTree<T>::isRightChild;
    using BinaryTree<T>::isRightOf;
    using BinaryTree<T>::leftRotate;
    using BinaryTree<T>::m_root;
    using BinaryTree<T>::rightRotate;
    using typename BinaryTree<T>::Node;
public:
    using BinaryTree<T>::printLevelOrder;

    ~RedBlackTree() {
        del(m_root);
    }

    template<typename... args_t>
    void insert(args_t&&... args) {
        auto node = new Node{std::forward<args_t>(args)...};
        if ( m_root == nullptr ) {
            m_root = node;
            recolor(m_root);
            return;
        }

        auto search = &m_root;
        auto parent = *search;
        while (*search != nullptr) {
            parent = *search;
            if ( node->m_data < (*search)->m_data )
                search = &(*search)->m_left;
            else if ( node->m_data > (*search)->m_data )
                search = &(*search)->m_right;
            else
                return;
        }
        *search = node;
        node->m_parent = parent;

        rebalanceInsert(node);
    }

    void remove(T const& data) {
        removeNode(internalFind(data));
    }

private:
    void removeNode(Node* node) {
        if ( node->m_parent == nullptr && node->m_left == nullptr && node->m_right == nullptr ) {
            m_root = nullptr;
            return;
        }

        Node* rep = nullptr;

        if ( node->m_left == nullptr && node->m_right == nullptr ) {
            rep = nullptr;
        } else if ( node->m_left == nullptr ) {
            rep = node->m_right;
        } else if ( node->m_right == nullptr ) {
            rep = node->m_left;
        } else {
            rep = getPredecessor(node);
            node->m_data = std::move(rep->m_data);
            removeNode(rep);
            return;
        }

        if ( isLeftChild(node) )
            node->m_parent->m_left = rep;
        else
            node->m_parent->m_right = rep;

        if ( ! isBlack(node) || ! isBlack(rep) ) {
            rep->m_red = false;
        } else {
            rebalanceRemove(node);
        }

        delete node;
    }

    void rebalanceLL(Node* node) {
        auto parent = getParent(node);
        auto grandparent = getGrandparent(node);
        rightRotate(grandparent);
        swap(parent->m_red, grandparent->m_red);
    }

    void rebalanceLR(Node* node) {
        auto parent = getParent(node);
        leftRotate(parent);
        rebalanceLL(node);
    }

    void rebalanceRR(Node* node) {
        auto parent = getParent(node);
        auto grandparent = getGrandparent(node);
        leftRotate(grandparent);
        swap(parent->m_red, grandparent->m_red);
    }

    void rebalanceRL(Node* node) {
        auto parent = getParent(node);
        auto grandparent = getGrandparent(node);
        rightRotate(parent);
        rebalanceRR(node);
    }

    void rebalanceInsert(Node* node) {
        if ( node == m_root ) {
            node->m_red = false;
            return;
        }
        if ( isBlack(node->m_parent) ) return;

        auto uncle = getUncle(node);
        auto parent = getParent(node);
        auto grandparent = getGrandparent(node);

        if ( ! isBlack(uncle) ) {
            uncle->m_red = false;
            getParent(node)->m_red = false;
            getGrandparent(node)->m_red = true;
            rebalanceInsert(getGrandparent(node));
        } else {
            if ( isLeftOf(parent, grandparent) ) {
                if ( isLeftOf(node, parent) )
                    rebalanceLL(node);
                else
                    rebalanceLR(node);
            } else {
                if ( isLeftOf(node, parent) )
                    rebalanceRL(node);
                else
                    rebalanceRR(node);
            }
        }
    }

    void rebalanceRemove(Node* node) {
        if ( node == nullptr || node->m_parent == nullptr ) return;
        auto sib = getSibling(nullptr, node->m_parent); // Node just got removed, but we work on the replaced node, so it's value isn't nullptr
        auto parent = getParent(node);

        if ( sib == nullptr ) {
            rebalanceRemove(node->m_parent);
            return;    
        }

        if ( isBlack(sib) ) {
            if ( sib == nullptr ) {
                rebalanceRemove(parent);
            } else if ( ! isBlack(sib->m_left) || ! isBlack(sib->m_right) ) {
                auto child = isBlack(sib->m_left) ? sib->m_right : sib->m_left;

                if ( isRightChild(sib) ) {
                    if ( isRightChild(child) ) {
                        sib->m_right->m_red = sib->m_red;
                        sib->m_red = parent->m_red;
                        leftRotate(parent); 
                    } else {
                        sib->m_left->m_red = parent->m_red;
                        rightRotate(sib);
                        leftRotate(parent);
                    }
                } else {
                    if ( isRightChild(child) ) {
                        sib->m_right->m_red = parent->m_red;
                        leftRotate(sib);
                        rightRotate(parent);
                    } else {
                        sib->m_left->m_red = sib->m_red;
                        sib->m_red = parent->m_red;
                        rightRotate(parent); 
                    }
                }

                parent->m_red = true;
            } else {
                sib->m_red = false;
                if ( isBlack(parent) )
                    rebalanceRemove(parent);
                else
                    parent->m_red = false;
            }
        } else {
            auto parent = getParent(node);
            if ( isLeftChild(sib) )
                rightRotate(parent);
            else
                leftRotate(parent);
            parent->m_red = true;
            sib->m_red = false;
            rebalanceRemove(node);
        }
    }

    bool isBlack(Node* node) {
        return ( node == nullptr ) || ( ! node->m_red );
    }

    void recolor(Node* node) {
        if ( node == nullptr ) return;
        node->m_red = ! node->m_red;
    }

};
