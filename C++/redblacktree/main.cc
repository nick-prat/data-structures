#include "rbtree.hh"

#include <iostream>

int main(int argc, char const** argv) {
    auto rbtree = RedBlackTree<int>();
    // rbtree.insert(1);
    // rbtree.printLevelOrder();
    // std::cout << '\n';
    // rbtree.insert(2);
    // rbtree.printLevelOrder();
    // std::cout << '\n';
    // rbtree.insert(3);
    // rbtree.printLevelOrder();
    // std::cout << '\n';
    // rbtree.insert(4);
    // rbtree.printLevelOrder();
    // std::cout << '\n';
    // rbtree.insert(5);
    // rbtree.printLevelOrder();
    // std::cout << '\n';
    // rbtree.insert(6);
    // rbtree.printLevelOrder();
    // std::cout << '\n';
    // rbtree.insert(7);
    // rbtree.printLevelOrder();
    // std::cout << '\n';
    // rbtree.insert(0);
    // rbtree.printLevelOrder();
    // std::cout << '\n';
    // rbtree.insert(11);
    // rbtree.printLevelOrder();
    // std::cout << '\n';
    // rbtree.insert(-3);
    // rbtree.printLevelOrder();

    // rbtree.insert(3);
    // rbtree.insert(21);
    // rbtree.insert(32);
    // rbtree.insert(15);

    rbtree.insert(1);
    rbtree.insert(2);
    rbtree.insert(3);
    rbtree.insert(4);
    rbtree.insert(5);
    rbtree.insert(0);
    rbtree.insert(6);
    rbtree.insert(7);
    rbtree.insert(8);

    rbtree.remove(4);
    rbtree.remove(6);

    // ? 5 is red at this point, need to fix that

    rbtree.printLevelOrder(std::cout);
}
